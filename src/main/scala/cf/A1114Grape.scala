//problem https://codeforces.com/contest/1114/problem/A
package cf

import scala.io.StdIn.readLine
import scala.math._

object A1114Grape extends App {
  val x::y::z::_ = readLine().split(" ").map(_.toInt).toList
  val a::b::c::_ = readLine().split(" ").map(_.toInt).toList

  def check1(a1: Int, b1: Int, c1: Int): Option[(Int, Int, Int)] = if (x <= a1) Some((a1 - x, b1, c1)) else None

  def check2(a1: Int, b1: Int, c1: Int): Option[(Int, Int, Int)] = if (y <= a1 + b1) Some((max(0, a1 - y), b1 + min(0, a1 - y), c1 )) else None

  def check3(a1: Int, b1: Int, c1: Int): Option[Unit] = if (z <= a1 + b1 + c1) Some(()) else None

  {
    for {
      (a1, b1, c1) <- check1(a, b, c)
      (a2, b2, c2) <- check2(a1, b1, c1)
      result       <- check3(a2, b2, c2)
    } yield result
  } match {
    case Some(_) => print("YES")
    case None    => print("NO")
  }

}
