//https://codeforces.com/contest/1144/problem/C
package cf

object C1144MixedSeq extends App {
  import scala.io.StdIn.{readLine, readInt}
  val n = readInt
  val arr1 = readLine.split(' ').map(_.toInt).groupBy(identity).mapValues(_.length)
  val arr2 = arr1.filter(_._2 == 2)
  val s1 = arr1.keys.toList.sorted
  val s2 = arr2.keys.toList.sorted.reverse
  if(s1.length + s2.length != n) {
    println("NO")
  } else {
    println("YES")
    println(s1.length)
    println(s1.mkString(" "))
    println(s2.length)
    println(s2.mkString(" "))
  }

}
