//problem https://codeforces.com/contest/1114/problem/B

package cf

import scala.io.StdIn.readLine

object B1114ArraySplit extends App {
  val n::m::k::_ = readLine().split(" ").map(_.toInt).toList
  val as = readLine().split(" ").map(BigInt(_)).toList

  val cuts = as.zipWithIndex.sortBy(_._1).takeRight(m * k).map(_._2).sorted.zipWithIndex.filter(x => x._2 % m == 0).map(_._1).tail

  println(as.sorted.takeRight(m * k).sum)
  println(cuts.mkString(" "))
}
