//https://codeforces.com/contest/1144/problem/B
package cf

object B1144Policarp extends App{
  import scala.io.StdIn.{readInt, readLine}
  val _ = readInt
  val arr = readLine.split(' ').map(_.toInt).toList.sorted
  val odd = arr filter {_ % 2 == 1}
  val even = arr filter {_ % 2 == 0}
  val forDrop = math.min(odd.length, even.length) + 1
  println(odd.dropRight(forDrop).sum + even.dropRight(forDrop).sum)
}
