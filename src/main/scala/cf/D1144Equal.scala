//https://codeforces.com/contest/1144/problem/D
package cf

object D1144Equal extends App {
  import scala.collection.mutable
  import scala.io.StdIn.{readInt, readLine}

  val n = readInt
  val arr = readLine.split(' ').map(_.toInt).toArray
  val freq = arr.groupBy(identity).mapValues(_.length).maxBy(_._2)._1
  val pivot = arr.zipWithIndex.find(_._1 == freq).get._2

  val res: mutable.ArrayBuffer[(Int, Int, Int)] = mutable.ArrayBuffer.empty

  for(i <- (0 until pivot).reverse) {
    if(arr(i) < freq) res.append((1, i, i + 1))
    else if (arr(i) > freq) res.append((2, i, i + 1))
  }

  for(i <- pivot + 1 until n) {
    if(arr(i) < freq) res.append((1, i, i - 1))
    else if (arr(i) > freq) res.append((2, i, i - 1))
  }


  println(res.length)
  println(res.map( x => s"${x._1} ${x._2 + 1} ${x._3 + 1}").mkString("\n"))

}
