//https://codeforces.com/contest/1144/problem/A
package cf

import scala.io.StdIn.{readInt, readLine}

object A1144Strings extends App {
  (0 until readInt()).map(_ => readLine.toCharArray.sorted) map{x =>
    x.map(_ - x.head).zipWithIndex.exists(x => x._1 != x._2)
  } foreach {if(_) println("No") else println("Yes")}
}

