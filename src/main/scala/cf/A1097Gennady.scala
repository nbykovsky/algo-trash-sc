//problem http://codeforces.com/contest/1097/problem/A?locale=en
package cf

import scala.io.StdIn.readLine
import cats.Traverse
import cats.instances.list._
import cats.instances.option._

/**
  * Very complex type safe implementation of very simple task
  */
object A1097Gennady extends App {

  trait Suit
  case object diamond extends Suit
  case object club extends Suit
  case object spade extends Suit
  case object heart extends Suit

  trait Rank
  case object two extends Rank
  case object tree extends Rank
  case object four extends Rank
  case object five extends Rank
  case object six extends Rank
  case object seven extends Rank
  case object eight extends Rank
  case object nine extends Rank
  case object t extends Rank
  case object j extends Rank
  case object q extends Rank
  case object k extends Rank
  case object a extends Rank

  case class Card(rank: Rank, suit: Suit)

  def getRank(s: String):Option[Rank] = {
    s.toList match {
      case '2'::_ => Some(two)
      case '3'::_ => Some(tree)
      case '4'::_ => Some(four)
      case '5'::_ => Some(five)
      case '6'::_ => Some(six)
      case '7'::_ => Some(seven)
      case '8'::_ => Some(eight)
      case '9'::_ => Some(nine)
      case 'T'::_ => Some(t)
      case 'J'::_ => Some(j)
      case 'Q'::_ => Some(q)
      case 'K'::_ => Some(k)
      case 'A'::_ => Some(a)
      case _      => None
    }
  }

  def getSuit(s: String):Option[Suit] = {
    s.toList match {
      case _::'D'::Nil => Some(diamond)
      case _::'C'::Nil => Some(club)
      case _::'S'::Nil => Some(spade)
      case _::'H'::Nil => Some(heart)
      case _           => None

    }
  }

  def getCard(s: String): Option[Card] =
    for {
      rank <- getRank(s)
      suit <- getSuit(s)
    } yield Card(rank, suit)

  val card = getCard(readLine())
  val cards = Traverse[List].sequence(readLine().split(" ").map(getCard).toList)

  {
    for {
      c <- card
      cs <- cards
      if cs.map(x => x.rank == c.rank || c.suit == x.suit).reduce((a,b) => a || b)
    } yield ()
  } match {
    case Some(_) => println("YES")
    case None => println("NO")
  }
}

/**
  * Simple implementation for submission
  */
object Solution extends App {
  val card = readLine()
  val cards = readLine().split(" ")
  if (cards.map(x => x(0) == card(0) || x(1) == card(1)).reduce((a, b) => a || b)) {
    println("YES")
  } else {
    println("NO")
  }

}