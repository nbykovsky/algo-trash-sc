// https://codeforces.com/contest/1152/problem/A

package cf.cf1152

object a extends App {
  import scala.io.StdIn.readLine
  import scala.math.min

  val _ = readLine
  def getArr: (Array[Int], Array[Int]) = readLine.split(" ").map(_.toInt).partition(_ % 2 == 0)
  val aa = getArr
  val bb = getArr

  val (aOdd, aEven) = (aa._1.length, aa._2.length)
  val (bOdd, bEven) = (bb._1.length, bb._2.length)

  println(min(aOdd, bEven) + min(aEven, bOdd))

}
