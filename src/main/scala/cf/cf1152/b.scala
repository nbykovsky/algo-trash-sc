package cf.cf1152

object b extends App{
  import scala.math.log
  import scala.io.StdIn.readInt

  def log2(x: Double) = log(x) / log(2)
  def binLength(x: Int) = if (x == 0) 1 else log2(x).toInt + 1
  def inversion(x: Int):Int = (~x) & ((1 << binLength(x)) - 1)

  /**
    * Returns a number of digits for the left most zero
    * 01101 -> 2
    * 10    -> 1
    */
  def asd(n: Int): Int =  {
    def helper(m: Int, acc: Int): Int = {
      if(m == 0) acc
      else helper(m >> 1, acc + 1)
    }
    helper(inversion(n), 0)
  }


  def iterate(x: Int, cnt: Int, acc: List[Int]): (Int, List[Int]) = {
    if (x + 1 == (1 << binLength(x))) (cnt - 1, acc)
    else if (cnt % 2 == 1) {
      val n = asd(x)
      //    println(s"n -> $n")
      iterate(x ^ ((1 << n) - 1), cnt + 1, n::acc)
    } else {
      //    println("Add 1")
      iterate(x + 1, cnt + 1, acc)
    }
  }

  val n = readInt()

  if (n + 1 == (1 << binLength(n))) {
    println(0)
  } else {
    val (cnt, acc) = iterate(n, 1, List())
    println(cnt)
    println(acc.reverse.mkString(" "))
  }

}
