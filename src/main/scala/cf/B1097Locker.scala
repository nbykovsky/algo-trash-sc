//problem http://codeforces.com/contest/1097/problem/B?locale=en
package cf

import scala.io.StdIn.readInt

object B1097Locker extends App{
  val n = readInt
  val turns = (1 to n).map(_ => readInt()).toList

  def helper(ls: List[Int], cur: Int): Boolean = {
    ls match {
      case Nil => cur == 0
      case x::xs => helper(xs, (cur + x) % 360) || helper(xs, (360 + cur - x) % 360)
    }
  }

  if (helper(turns, 0)) println("YES")
  else println("NO")

}
