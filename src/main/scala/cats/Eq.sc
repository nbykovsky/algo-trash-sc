import cats.Eq
import cats.instances.eq._
import cats.syntax.eq._


case class Cat(name: String, age: Int, color: String)

implicit val catEq: Eq[Cat] = Eq.instance[Cat]((cat1, cat2) => cat1.name == cat2.name)


val cat1 = Cat("Garfield",   38, "orange and black")
val cat2 = Cat("Garfield", 33, "orange and black")

cat1 === cat2

val optionCat1 = Option(cat1)
val optionCat2 = Option.empty[Cat]
val optionCat3 = Option(cat2)

