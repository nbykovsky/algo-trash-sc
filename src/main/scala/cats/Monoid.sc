
/* Custom implementation of Monoid and Semigroup */
trait MySemigroup[A] {
  def combine(x: A, y: A): A
}

trait MyMonoid[A] extends MySemigroup[A]{
  def empty: A
}

object MyMonoid {

  def apply[A](implicit monoid: MyMonoid[A]):MyMonoid[A] = monoid

}

object MonoidInstances {

  implicit val boolMonoidAnd: MyMonoid[Boolean] = new MyMonoid[Boolean]() {
    override def empty = true

    override def combine(x: Boolean, y: Boolean):Boolean = x && y
  }

  implicit def setMonoid[A]:MyMonoid[Set[A]] = new MyMonoid[Set[A]] {
    def empty:Set[A] = Set.empty[A]

    def combine(x: Set[A], y: Set[A]): Set[A] = x union y
  }
}

import MonoidInstances._

val intSetMonoid = MyMonoid[Set[Int]]

intSetMonoid.combine(Set(1, 2), Set(2, 3))

/* Build in implementation */
import cats.instances.list._
import cats.instances.int._
import cats.instances.option._
import cats.Monoid

import cats.syntax.monoid._

def add[A: Monoid](items: List[A]): A = items.foldLeft(Monoid[A].empty)(_ |+| _)

case class Order(totalCost: Double, quantity: Double)

add(List(1,2,3))
add(List(Option(1), Option(2)))

implicit val orderMonoid: Monoid[Order] = new Monoid[Order] {
  def empty = Order(0,0)
  def combine(x: Order, y: Order) = Order(x.totalCost + y.totalCost, x.quantity + y.quantity)
}

add(List(Order(1,1), Order(2,2)))