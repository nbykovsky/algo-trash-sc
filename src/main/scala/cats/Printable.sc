

case class Cat(name: String, age: Int, color: String)

/*Printable type class for String representation of objects (like toString) */
trait Printable[A] {

  def format(a: A):String

}


object PrintableInstances {

  implicit val printableInt = new Printable[Int] {
    override def format(a: Int):String = a.toString
  }

  implicit val printableString = new Printable[String] {
    override def format(a: String):String = a.toString
  }

  implicit val printableCat = new Printable[Cat] {
    override def format(a: Cat): String = s"${a.name} is a ${a.age} old ${a.color} cat"
  }
}

object Printable {

  def format[A](a: A)(implicit pr: Printable[A]):String = pr.format(a)

  def print[A](a: A)(implicit pr: Printable[A]):Unit =  println(pr.format(a))
}


object PrintableSyntax {

  implicit class PrintableOps[A](a: A) {
    def format(implicit p: Printable[A]): String = p.format(a)

    def print(implicit p: Printable[A]): Unit = println(p.format(a))
  }
}

import PrintableInstances._
import PrintableSyntax._

Printable.format(1234)
Printable.format("ABCD")


Printable.format(Cat("a", 25, "b"))

Cat("a", 25, "b").print

import cats._
import cats.implicits._


/* Implement the same with built in type Show*/
implicit val catShow: Show[Cat] = Show.show(a => s"${a.name} is a ${a.age} old ${a.color} cat")

Cat("a", 25, "b").show