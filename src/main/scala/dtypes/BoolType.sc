
object BoolType {

  sealed trait Boolean {
    type not <: Boolean
    type or[that <: Boolean] <: Boolean
    type and[that <: Boolean] = this.type#not#or[that#not]#not
  }


  sealed trait True extends Boolean {
    type not = False
    type or[that <: Boolean] = True
  }

  sealed trait False extends Boolean {
    type not = True
    type or[that <: Boolean] = that
  }


}

import BoolType._

implicitly[True#and[False] =:= False]
implicitly[False#and[False] =:= False]
implicitly[False#and[True] =:= False]
implicitly[True#and[True] =:= True]


