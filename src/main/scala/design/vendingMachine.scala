/*
Vending Machine design
 */
package design

import cats._
import cats.implicits._

import scala.concurrent.Future


object vendingMachine {


  sealed trait Good {
    val price: Int
  }

  case object Coke extends Good {
    val price = 20
  }

  case object Gum extends Good {
    val price = 5
  }

  sealed trait Coin {
    val value: Int
  }

  case object C_1 extends Coin {
    val value = 1
  }

  case object C_5 extends Coin {
    val value = 1
  }

  trait CoinsRepository[F[_]] {

    /**
      * Adds coins to the repository
      */
    def addCoin(money: Coin): F[Unit]

    /**
      * Checks whether amount could be composed using available coins
      */
    def checkMoney(amount: Int): F[Unit]

    /**
      * Returns amount in coins
      */
    def complete(amount: Int): F[Seq[Coin]]

  }

  trait GoodsRepository[F[_]] {

    /**
      * Adds several goods into repository
      */
    def addGoods(goods: Seq[Good]): F[Unit]

    /**
      * Check whether good is available in repo
      */
    def checkGood(good: Good): F[Unit]

    /**
      * Fetches good from repo
      */
    def getGood(good: Good): F[Good]
  }

  class DomainModel[F[_]: Monad](
                                  val goods: GoodsRepository[F],
                                  val coins: CoinsRepository[F]) {

    def acceptCoin(coin: Coin): F[Unit] = coins.addCoin(coin)


    def getGood(good: Good): F[(Good, Seq[Coin])] = {
      for {
        _ <- goods.checkGood(good) // checking that good is present
        _ <- coins.checkMoney(good.price) // checking that we collected required amount and able to return change
        g <- goods.getGood(good) // Fetch good from repo
        c <- coins.complete(good.price) // get change
      } yield (g, c)
    }

  }

  object FutureApi(val goods: GoodsRepository[Future], val coins: CoinsRepository[Future]) {

    import cats._
    import cats.implicits._
    import scala.concurrent.ExecutionContext.Implicits.global



    val domainModel: DomainModel[Future] = new DomainModel[Future](goods, coins)

    def acceptCoin(coin:Coin):Future[Unit] = domainModel.acceptCoin(coin)

  }

  object main extends App {


  }


}