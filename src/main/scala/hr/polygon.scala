//https://www.hackerrank.com/challenges/lambda-march-compute-the-area-of-a-polygon/problem
package hr

object polygon {
  import scala.io.StdIn.{readInt, readLine}

  def main(args: Array[String]) {

    val n = readInt
    val ps = (0 until n).map(_ => readLine.split(" ").map(_.toDouble)).map(x =>(x(0), x(1)))
    val r = ps.zip(ps.tail :+ ps.head).foldLeft(0:Double){(a,e) =>
      e match {
        case ((x1, y1), (x2, y2)) =>
          val area = scala.math.abs((x1 - x2) * (y1 + y2) / 2)
          if (x1 > x2) a + area
          else a - area
      }
    }

    println((r * 10).floor / 10)
  }
}
