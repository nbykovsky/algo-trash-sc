package cj

object Aforegone extends App{
  import scala.io.StdIn.{readLine, readInt}
  (1 to readInt).foreach { i =>
    val l = readLine.toCharArray
    val n1 = l.map(x => if(x == '4') '2' else x).mkString
    val n2 = l.map(x => if(x != '4') '0' else '2').dropWhile(_ == '0').mkString
    println(s"Case #$i: $n1 $n2")
    if(n1.toInt + n2.toInt != l.mkString.toInt | n1.contains('4') | n2.contains('4'))  println("WRONG")
  }
}
