package algostepic

import scala.io.StdIn.readLine

object knapsak extends App{
  val n::w::Nil = readLine().split(" ").map(_.toInt).toList

  case class Item(cost: Double, volume: Double)
  val v = (1 to n).map(_ => readLine().split(" ").map(_.toDouble)).map(x => Item(x(0)/x(1), x(1))).sortBy(_.cost).reverse

  case class Acc(totalCost: Double, capacity: Double)

  def adder(acc: Acc, item: Item): Acc = {
    if(acc.capacity == 0) acc
    else if (acc.capacity >= item.volume){
      Acc(acc.totalCost + item.cost * item.volume, acc.capacity - item.volume )
    } else {
      Acc(acc.totalCost + item.cost * acc.capacity, 0)
    }
  }

  println(v.foldLeft(Acc(0, w))(adder).totalCost)

}
