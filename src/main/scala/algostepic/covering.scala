// problem https://stepik.org/lesson/13238/step/9?unit=3424
package algostepic

import scala.io.StdIn.{readInt, readLine }

object covering extends App {
  val n = readInt()
  val lr = (1 to n) map {_ => readLine().split(" ")} map {x => (x(0).toInt, x(1).toInt)} sortBy(_._2)

  def getMin(rest: List[(Int, Int)], acc: List[Int]): List[Int] = {
    rest match {
      case Nil => acc
      case (_,r)::xs => getMin(xs.dropWhile(_._1 <= r), r::acc)
    }
  }

  val points = getMin(lr.toList, List())
  println(points.length)
  println(points.reverse.mkString(" "))
}
