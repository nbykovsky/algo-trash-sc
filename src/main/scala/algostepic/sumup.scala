package algostepic

import scala.io.StdIn.readInt
import scala.math._

object sumup extends App{
  val n = readInt()

  val m = {
    val x = floor(sqrt(n * 2)).toInt - 1
    if (n - ((x * (x + 1)) / 2).toInt <= x)  x - 1
     else x
  }

  val ans = (1 to m).toList :+ (n - ((m * (m + 1)) / 2).toInt)
  if (n < 3) {
    println(1)
    println(n)
  } else {
    println(ans.length)
    println(ans.mkString(" "))
  }

}
